import Vue from 'vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap.js'
import 'bootstrap/dist/js/bootstrap.bundle.js'
import '../src/assets/style/reset.css'
import '../src/assets/style/all.css'
import '../src/assets/style/sass/app.css'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import App from './App.vue'

// import style
import 'swiper/swiper.min.css'

import i18n from './i18n'
Vue.use(VueAwesomeSwiper, /* { default options with global component } */);



Vue.config.productionTip = false

new Vue({
  i18n,
  render: h => h(App)
}).$mount('#app')
